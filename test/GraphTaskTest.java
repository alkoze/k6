import static org.junit.Assert.*;
import org.junit.Test;
import java.util.*;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {

   @Test (timeout=20000)
   public void test_update_of_vertex_list() {
      GraphTask.Graph a = new GraphTask().new Graph("a");
      GraphTask.Vertex V1 = a.createVertex("v1", 2);
      GraphTask.Vertex V2 = a.createVertex("v2", 2);
      assertEquals(2, a.getVertexlist().size());
   }
   @Test (timeout=20000)
   public void test_update_of_arc_list() {
      GraphTask.Graph a = new GraphTask().new Graph("a");
      GraphTask.Vertex V1 = a.createVertex("v1", 2);
      GraphTask.Vertex V2 = a.createVertex("v2", 2);
      a.createArc("v1_v2", V1, V2);
      assertEquals("v1_v2", V1.getArcList().get(0).toString());
   }
   @Test (timeout=20000)
   public void test_path_finding_1() {
      GraphTask.Graph a = new GraphTask().new Graph("a");
      GraphTask.Vertex V1 = a.createVertex("v1", 1);
      GraphTask.Vertex V2 = a.createVertex("v2", 2);
      a.createArc("v1_v2", V1, V2);
      LinkedList<LinkedList<GraphTask.Vertex>> paths = a.findpaths(a.getVertexlist().get(0), a.getVertexlist().get(1));
      assertEquals(0, paths.size());
   }
   @Test (timeout=20000)
   public void test_path_finding_2() {
      GraphTask.Graph a = new GraphTask().new Graph("a");
      GraphTask.Vertex v1 = a.createVertex("v1", 1);
      GraphTask.Vertex v2 = a.createVertex("v2", 2);
      GraphTask.Vertex v3 = a.createVertex("v3", 1);
      GraphTask.Vertex v4 = a.createVertex("v4", 1);
      a.createArc("v1_v3", v1, v3);
      a.createArc("v2_v3", v2, v3);
      a.createArc("v2_v4", v2, v4);
      a.createArc("v3_v1", v3, v1);
      a.createArc("v3_v2", v3, v2);
      a.createArc("v3_v4", v3, v4);
      a.createArc("v4_v2", v4, v2);
      a.createArc("v4_v3", v4, v3);
      LinkedList<LinkedList<GraphTask.Vertex>> paths = a.findpaths(a.getVertexlist().get(0), a.getVertexlist().get(3));
      assertEquals(1, paths.size());
      assertEquals("v1 v3 v4", a.pathTostring(paths.get(0)));
   }
   @Test (timeout=20000)
   public void test_path_finding_3() {
      GraphTask.Graph a = new GraphTask().new Graph("a");
      GraphTask.Vertex v1 = a.createVertex("v1", 1);
      GraphTask.Vertex v2 = a.createVertex("v2", 1);
      GraphTask.Vertex v3 = a.createVertex("v3", 1);
      GraphTask.Vertex v4 = a.createVertex("v4", 1);
      a.createArc("v1_v3", v1, v3);
      a.createArc("v2_v3", v2, v3);
      a.createArc("v2_v4", v2, v4);
      a.createArc("v3_v1", v3, v1);
      a.createArc("v3_v2", v3, v2);
      a.createArc("v3_v4", v3, v4);
      a.createArc("v4_v2", v4, v2);
      a.createArc("v4_v3", v4, v3);
      LinkedList<LinkedList<GraphTask.Vertex>> paths = a.findpaths(a.getVertexlist().get(0), a.getVertexlist().get(3));
      assertEquals(2, paths.size());
      assertEquals("v1 v3 v4", a.pathTostring(paths.get(0)));
      assertEquals("v1 v3 v2 v4", a.pathTostring(paths.get(1)));
   }

}

