import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      Vertex v1 = g.createVertex("v1", 1);
      Vertex v2 = g.createVertex("v2", 2);
      Vertex v3 = g.createVertex("v3", 1);
      Vertex v4 = g.createVertex("v4", 1);
      g.createArc("v1_v3", v1, v3);
      g.createArc("v2_v3", v2, v3);
      g.createArc("v2_v4", v2, v4);
      g.createArc("v3_v1", v3, v1);
      g.createArc("v3_v2", v3, v2);
      g.createArc("v3_v4", v3, v4);
      g.createArc("v4_v2", v4, v2);
      g.createArc("v4_v3", v4, v3);
      System.out.println (g.toString());
      LinkedList<LinkedList<Vertex>> paths = g.findpaths(g.vertexlist.get(0), g.vertexlist.get(3));
      g.printpaths(paths);
      g.resetGraph();
      System.out.println("+++++++++++++++++++++++++++++++++++++");
      v1 = g.createVertex("v1", 1);
      v2 = g.createVertex("v2", 1);
      v3 = g.createVertex("v3", 3);
      v4 = g.createVertex("v4", 1);
      Vertex v5 = g.createVertex("v5", 1);
      g.createArc("v1_v2", v1, v2);
      g.createArc("v1_v3", v1, v3);
      g.createArc("v1_v4", v1, v4);
      g.createArc("v1_v5", v1, v5);
      g.createArc("v2_v3", v2, v3);
      g.createArc("v2_v4", v2, v4);
      g.createArc("v2_v5", v2, v5);
      g.createArc("v3_v4", v3, v4);
      g.createArc("v3_v5", v3, v5);
      g.createArc("v4_v5", v4, v5);
      g.createArc("v5_v2", v5, v2);
      g.createArc("v5_v3", v5, v3);
      g.createArc("v5_v4", v5, v4);
      System.out.println (g.toString());
      LinkedList<LinkedList<Vertex>> paths1 = g.findpaths(g.vertexlist.get(0), g.vertexlist.get(3));
      g.printpaths(paths1);
      g.resetGraph();
      System.out.println("+++++++++++++++++++++++++++++++++++++");
      v1 = g.createVertex("v1", 0);
      v2 = g.createVertex("v2", 0);
      v3 = g.createVertex("v3", 1);
      g.createArc("v1_v2", v1, v2);
      g.createArc("v2_v3", v2, v3);
      System.out.println (g.toString());
      LinkedList<LinkedList<Vertex>> paths2 = g.findpaths(g.vertexlist.get(0), g.vertexlist.get(2));
      g.printpaths(paths2);
      g.resetGraph();
      System.out.println("+++++++++++++++++++++++++++++++++++++");
      v1 = g.createVertex("v1", 1);
      v2 = g.createVertex("v2", 0);
      v3 = g.createVertex("v3", 1);
      v4 = g.createVertex("v4", 1);
      g.createArc("v1_v2", v1, v2);
      g.createArc("v2_v3", v2, v3);
      g.createArc("v1_v4", v1, v4);
      g.createArc("v4_v3", v4, v3);
      System.out.println (g.toString());
      LinkedList<LinkedList<Vertex>> paths3 = g.findpaths(g.vertexlist.get(0), g.vertexlist.get(2));
      g.printpaths(paths3);
      g.resetGraph();
      System.out.println("+++++++++++++++++++++++++++++++++++++");
      v1 = g.createVertex("v1", 0);
      v2 = g.createVertex("v2", 1);
      g.createArc("v1_v2", v1, v2);
      g.createArc("v2_v1", v2, v1);
      System.out.println(g.toString());
      LinkedList<LinkedList<Vertex>> paths4 = g.findpaths(g.vertexlist.get(0), g.vertexlist.get(1));
      g.printpaths(paths4);
      g.resetGraph();
      System.out.println("+++++++++++++++++++++++++++++++++++++");
//      g.createRandomSimpleGraph(50, 300);
//      System.out.println(g.toString());
//      g.printpaths(g.findpaths(g.vertexlist.get(0), g.vertexlist.get(32)));





      // TODO!!! Your experiments here
   }
   // TODO!!! add javadoc relevant to your problem

   /** Olgu antud geograafiliste punktide graaf,
    * mille iga tipuga on seotudvastava punkti kõrgus merepinnast.
    * Kirjutada graafi laiuti läbimisel põhinev algoritm, mis kontrollib,
    * kas kahe antud punkti vahel leidub samakõrgustee, st. tee, millel asuvad punktid on kõik ühe ja sama kõrgusega.
    */

   /** Height - variable to comapre height of different vertexes
    * arcList - list of arcs connected to vertex
    */
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int height;
      private int info = 0;
      private LinkedList<Arc> arcList = new LinkedList<>();
      // You can add more fields, if needed

      Vertex (String s, Vertex v, Arc e, int h) {
         id = s;
         next = v;
         first = e;
         height = h;
      }

      public LinkedList<Arc> getArcList(){
         return arcList;
      }

      Vertex (String s) {
         this (s, null, null, 0);
      }

      @Override
      public String toString() {
         return id;
      }

      /** Method to add arcs to param arcList in vertex. */
      public void addArc (Arc a){
         arcList.push(a);
      }

      // TODO!!! Your Vertex methods here!
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      // TODO!!! Your Arc methods here!
   } 

   /** vertexlist contains all vertexes in the graph
    */
   class Graph {

      private String id;
      private Vertex first;
      private LinkedList<Vertex> vertexlist = new LinkedList<>();
      private int info = 0;
      // You can add more fields, if needed

      public LinkedList<Vertex> getVertexlist(){
         return vertexlist;
      }

      /** Method to restore graph first look.
       */
      public void resetGraph(){
         first = null;
         vertexlist.clear();
         info = 0;
      }

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }
      /** Method adds new vertex to vertexList in graph.
       */
      void addVertex (Vertex v){
         vertexlist.add(v);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append(" ");
            sb.append(v.height);
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid, int height) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         res.height = height;
         addVertex(res);
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         from.addArc(res);
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            Random random = new Random();
            varray [i] = createVertex ("v" + String.valueOf(n-i), random.nextInt(3));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */

      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j) 
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      // TODO!!! Your Graph methods here! Probably your solution belongs here.

      /** Check height of all vertexes in path and delete paths, with different heights.
       * @param path List of all paths
       */
      public void checkHeight(LinkedList<LinkedList<Vertex>> path){
         LinkedList<LinkedList<Vertex>> copyPath = (LinkedList) path.clone();
         for (LinkedList<Vertex> p: copyPath) {
            for (Vertex v: p){
               int nominalHeight = p.getFirst().height;
               if (nominalHeight != v.height){
                  path.remove(p);
               }
            }
         }
      }
      /** Check if current vertex is present in path
       * @param x - current vertex
       * @param path - current path
       * */
      public boolean isNotVisited(Vertex x, LinkedList<Vertex> path){
         return !path.contains(x);
      }

      /** Method to find all possible paths from source vertex to destination vertex.
       * @param start - source vertex
       * @param fin - destination vertex
       */

      public LinkedList<LinkedList<Vertex>> findpaths (Vertex start, Vertex fin) {
         LinkedList<LinkedList<Vertex>> pathWithfin = new LinkedList<>();
         LinkedList<LinkedList<Vertex>> queue = new LinkedList<>();
         LinkedList<Vertex> path = new LinkedList<>();
         path.add(start);
         queue.add(path);
         while (!queue.isEmpty()){
            path = queue.peek();
            queue.pop();
            Vertex last = path.getLast();
            if (last.id.equals(fin.id)){
               pathWithfin.add(path);
            }
            for (Arc cur: last.arcList) {
               if (isNotVisited(cur.target, path)) {
                  LinkedList <Vertex> newpath = (LinkedList) path.clone();
                  newpath.add(cur.target);
                  queue.add(newpath);
               }
            }
         }
         checkHeight(pathWithfin);
         return pathWithfin;
      }

      /** Method to print correct paths if there are any
       * @param paths - correct paths
       */

      public void printpaths (LinkedList<LinkedList<Vertex>> paths){
         if (paths.size() != 0){
            System.out.println("Paths with same height: ");
            for (LinkedList<Vertex> p: paths) {
               System.out.println(pathTostring(p));
               System.out.println();
               }
            } else {
            System.out.println("There are no paths with same height.");
         }
      }

      /** Method to convert list of vertexes to string
       * @param path - list of vertexes
       */
      public String pathTostring (LinkedList<Vertex> path){
         String s ="";
         for (Vertex v : path) {
            s += v.toString() + " ";
         }
         s = s.trim();
         return s;
      }
   }
}


//Allikad https://www.geeksforgeeks.org/print-paths-given-source-destination-using-bfs/